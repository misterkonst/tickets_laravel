<?php

use Carbon\CarbonPeriod;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Schema;

class FakeSeeds extends Migration
{
    /** @var array  */
    private $costs = [];
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $this->costs = $this->getDefaultCostByType();
        $data = $this->defaultData();

        $this->createDefaultTicket();

        foreach ($data as $date => $value) {
            foreach ($value['tickets'] as $ticket){
                $Ticket = new \App\Models\Tickets();
                $Ticket->from_date = str_replace('{date}', $date, $ticket['time']);;
                $Ticket->type = $ticket['type'];
                if($ticket['type'] === \App\Models\Tickets::TICKET_TYPE_HOLIDAY){
                    $Holiday = new \App\Models\Holidays();
                    $Holiday->date = $date;
                    $Holiday->save();
                    echo 'H';
                }
                $Ticket->price = (isset($ticket['price'])) ? $ticket['price'] : $this->costs[$ticket['type']];
                $Ticket->save();
                echo '+';
            }

        }
        echo PHP_EOL;
    }



    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        \App\Models\Tickets::truncate();
        \App\Models\Holidays::truncate();
    }

    private function createDefaultTicket(){
        $Model = new \App\Models\Tickets();
        $Model->from_date = '1970-01-01 00:00:00';
        $Model->price = $this->costs[\App\Models\Tickets::TICKET_TYPE_BASIC];
        $Model->type = \App\Models\Tickets::TICKET_TYPE_BASIC;
        $Model->save();
    }

    private function getDefaultCostByType(): array
    {
        return [
            \App\Models\Tickets::TICKET_TYPE_BASIC => 460,
            \App\Models\Tickets::TICKET_TYPE_WEEK_DAY => 590,
            \App\Models\Tickets::TICKET_TYPE_HOLIDAY => 680,
            \App\Models\Tickets::TICKET_TYPE_DATE => 750,
        ];
    }

    private function defaultData(): array
    {
        return [
            '2021-08-16' => [
                'tickets' => [
                    [
                        'type' => \App\Models\Tickets::TICKET_TYPE_WEEK_DAY,
                        'time' => '{date} 00:00',
                        'price' => 700,
                    ],
                ]
            ],
            '2021-08-17' => [
                'tickets' => [],
            ],
            '2021-08-18' => [
                'tickets' => [],
            ],
            '2021-08-19' => [
                'tickets' => [],
            ],
            '2021-08-20' => [
                'tickets' => [
                    [
                        'type' => \App\Models\Tickets::TICKET_TYPE_HOLIDAY,
                        'time' => '{date} 17:00'
                    ],
                ]
            ],
            '2021-08-21' => [
                'tickets' => [
                    [
                        'type' => \App\Models\Tickets::TICKET_TYPE_HOLIDAY,
                        'time' => '{date} 12:00',
                    ],
                ]
            ],
            '2021-08-22' => [
                'tickets' => [
                    [
                        'type' => \App\Models\Tickets::TICKET_TYPE_WEEK_DAY,
                        'time' => '{date} 12:00'
                    ],
                    [
                        'type' => \App\Models\Tickets::TICKET_TYPE_DATE,
                        'time' => '{date} 18:00'
                    ]
                ]
            ],
        ];
    }
}
