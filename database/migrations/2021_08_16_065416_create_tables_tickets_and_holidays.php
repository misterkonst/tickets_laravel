<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTablesTicketsAndHolidays extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tickets', function (Blueprint $table) {
            $table->id();
            $table->dateTime('from_date')->nullable();
            $table->integer('price')->default(0);
            $table->integer('type')->comment('1-Стандартный 2-День недели 3-Праздничный 4-на дату')->index();
            $table->index(['from_date', 'type']);
        });
        Schema::create('holidays', function (Blueprint $table) {
            $table->id();
            $table->dateTime('date')->index();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tickets');
        Schema::dropIfExists('holidays');
    }
}
