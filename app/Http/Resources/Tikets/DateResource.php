<?php

namespace App\Http\Resources\Tikets;

use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Carbon;

class DateResource extends JsonResource
{
    protected $res = [];

    public function __construct($resource)
    {
        $this->prepareData($resource);

        parent::__construct($resource);
    }

    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {

        return $this->res;
    }

    private function prepareData($resource)
    {
        foreach ($resource as $item) {
            $tickets = [];
            $countTickets = count($item['tickets']);
            for ($i = 0; $i < count($item['tickets']); $i++) {
                $name = null;
                if ($countTickets >= 2) {
                    if ($i === array_key_first($item['tickets'])) {
                        $name = 'до ' . Carbon::parse($item['tickets'][$i + 1]['time'])->format('H:i');
                    }
                    if (array_key_first($item['tickets']) < $i && $i <= array_key_last($item['tickets'])) {
                        $timeStart = Carbon::parse($item['tickets'][$i]['time'])->format('H:i');
                        $timeEnd = (isset($item['tickets'][$i + 1])) ? Carbon::parse($item['tickets'][$i + 1]['time'])->format('H:i') : '00:00';
                        $name = "от $timeStart до $timeEnd";
                    }
                    if ($i === array_key_last($item['tickets'])) {
                        $name = 'от ' . Carbon::parse($item['tickets'][$i]['time'])->format('H:i');
                    }
                }
                $tickets[] = [
                    'name' => $name,
                    'price' => $item['tickets'][$i]['price'],
                ];
            }

            $this->res[] = [
                'date' => $item['date'],
                'tickets' => $tickets,
            ];
        }
    }
}
