<?php

namespace App\Http\Controllers\Tickets;

use App\Models\Holidays;
use App\Models\Tickets;
use Carbon\CarbonPeriod;
use Exception;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Support\Carbon;

abstract class AbstractTicketClass
{
    /** Start week day  @var string */
    public $startDate;
    /** End week day @var  string */
    public $endDate;
    /** @var array */
    protected $tickets = [];
    /** @var null */
    protected $basicTicket = null;
    /** @var array */
    public $holidays = [];

    /**
     * @param string $startDate
     * @return mixed
     */
    abstract function initialize(string $startDate);

    /**
     * @return mixed
     */
    abstract function prepareData();

    /**
     * @return array
     */
    protected function getHolidays(): array
    {
        $data = [];
        $model = Holidays::where('date', '>=', $this->startDate)
            ->where('date', '<', $this->endDate)
            ->get()
            ->pluck('date');
        foreach ($model as $value) {
            $data[] = $value->format('Y-m-d');
        }

        $this->holidays = $data;

        return $this->holidays;
    }

    /**
     * @param string $date
     */
    protected function setDiffDates(string $date)
    {
        try {
            $now = Carbon::parse($date);
            $this->startDate = $now->startOfWeek()->format('d-m-Y');
        } catch (Exception $err) {
            throw new HttpResponseException(
                response()->json(['errors' => ['date' => ['Bad datetime format']]], 500, [], JSON_UNESCAPED_UNICODE));
        }

        try {
            $this->endDate = $now->endOfWeek()->format('d-m-Y');
        } catch (Exception $err) {
            throw new HttpResponseException(
                response()->json(['errors' => ['date' => ['Bad datetime format']]], 500, [], JSON_UNESCAPED_UNICODE));
        }
    }

    /**
     * @return array
     */
    protected function prepareDatePeriod(): array
    {
        $data = [];
        $startDate = Carbon::parse($this->startDate);
        $endDate = Carbon::parse($this->endDate);
        $period = CarbonPeriod::create($startDate->format('d-m-Y'), $endDate->format('d-m-Y'));
        foreach ($period as $date) {
            $data[] = $date->format('d.m.Y');
        }
        return $data;
    }

    protected function setBasicTicket()
    {
        $this->basicTicket = Tickets::where('type', Tickets::TICKET_TYPE_BASIC)->first();
    }
}
