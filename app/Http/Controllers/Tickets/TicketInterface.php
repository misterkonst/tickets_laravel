<?php

namespace App\Http\Controllers\Tickets;

interface TicketInterface
{
    /**
     * @param string $startDate
     * @return mixed
     */
    public function list(string $startDate);
}
