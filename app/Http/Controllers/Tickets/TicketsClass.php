<?php

namespace App\Http\Controllers\Tickets;

use App\Models\Tickets;
use Illuminate\Support\Carbon;

class TicketsClass extends AbstractTicketClass implements TicketInterface
{
    /**
     * @param string $startDate
     * @return array
     */
    public function list(string $startDate): array
    {
        $this->initialize($startDate);

        return $this->prepareData();
    }

    /**
     * @return array
     */
    public function prepareData(): array
    {
        $period = $this->prepareDatePeriod();

        return $this->prepareTickets($period);
    }

    /**
     * @param $period
     * @return array
     */
    private function prepareTickets($period): array
    {
        $data = [];

        foreach ($period as $date) {
            $model = Tickets::whereDate('from_date', Carbon::parse($date)->format('Y-m-d'))
                ->orWhere('type', Tickets::TICKET_TYPE_BASIC)
                ->orderBy('from_date', 'ASC')
                ->orderBy('type', 'ASC')
                ->get()
                ->toArray();
            if (count($model) === 0) {
                $data[] = [
                    'date' => $date,
                    'tickets' => [
                        'name' => null,
                        'price' => $this->basicTicket->price
                    ]
                ];
                continue;
            }

            $data[] = [
                'date' => $date,
                'tickets' => array_values($this->sortTickets($model)),
            ];
        }

        return $data;
    }

    /**
     * @param $model
     * @return array
     */
    private function sortTickets($model): array
    {
        $tickets = [];
        $beforeTicketTime = null;
        $beforeTicketType = 0;
        for ($i = 0; $i <= count($model) - 1; $i++) {
            $key = $i;
            if ($beforeTicketTime !== null && $beforeTicketTime === Carbon::parse($model[$i]['from_date'])->format('Hi')) {
                unset($tickets[$i - 1]);
                $key = $i - 1;
            }
            if ($beforeTicketType <= $model[$i]['type']) {
                $tickets[$key] = [
                    'time' => Carbon::parse($model[$i]['from_date'])->format('d-m-Y H:i:s'),
                    'price' => $model[$i]['price'],
                ];
            }
            $beforeTicketTime = Carbon::parse($model[$i]['from_date'])->format('Hi');
            $beforeTicketType = $model[$i]['type'];
        }

        return $tickets;
    }

    /**
     * @param string $startDate
     */
    public function initialize(string $startDate)
    {
        $this->setDiffDates($startDate);

        $this->getData();
    }

    private function getData()
    {
        $this->getHolidays();

        $this->setBasicTicket();
    }


}
