<?php

namespace App\Http\Controllers\Tickets;

use App\Http\Controllers\Controller;
use App\Http\Requests\Tickets\GetTicketRequest;
use App\Http\Resources\Tikets\DateResource;
use Carbon\Carbon;

class TicketsController extends Controller
{
    /**
     * @required param date
     * @param GetTicketRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getTickets(GetTicketRequest $request)
    {
        $now = Carbon::now();
        $result = (new TicketsClass())->list($now->startOfWeek()->format('Y-m-d'));

        return response()->json(new DateResource($result), 200, [], JSON_UNESCAPED_UNICODE);
    }
}
