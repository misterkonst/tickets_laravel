<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * @property mixed date
 */
class Holidays extends Model
{
    use HasFactory;

    protected $table = 'holidays';

    public $timestamps = false;

    protected $dates = ['date'];
}
