<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property mixed from_date
 * @property mixed type
 * @property mixed price
 */
class Tickets extends Model
{
    protected $table = 'tickets';

    public $timestamps = false;

    /** @var int */
    public const TICKET_TYPE_BASIC = 1;
    /** @var int */
    public const TICKET_TYPE_WEEK_DAY = 2;
    /** @var int */
    public const TICKET_TYPE_HOLIDAY = 3;
    /** @var int */
    public const TICKET_TYPE_DATE = 4;

    public function getTypes(): array
    {
        return [
            self::TICKET_TYPE_BASIC => 'Базовый билет',
            self::TICKET_TYPE_WEEK_DAY => 'Билет на день недели (пн, вт, ср, чт, пт, сб, вс)',
            self::TICKET_TYPE_HOLIDAY => 'Билет на праздничный день',
            self::TICKET_TYPE_DATE => 'Билет на конкретную дату',
        ];
    }
}
